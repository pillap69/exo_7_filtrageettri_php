<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <?php 
        require_once 'includes/head.php';   
        require_once './mesClasses/Cvisiteurs.php'; 
        require_once './mesClasses/Ctri.php';
    
        session_start();
    ?>
    
    <body>
        <?php       
        $ovisiteurs = new Cvisiteurs();
        $tabVilles = $ovisiteurs->getVilleVisiteur();
        //var_dump($tab);
        ?>
        <br>
        <br>
    <div class="container">
         
         <form class="form-inline" method="POST" action="formRechVisiteur.php">
            <div class="form-group ">
                <span class="glyphicon glyphicon-search"></span>
                &nbsp; 
                <label for="ville">Choisir la localité :</label>
                &nbsp;
                <select name="ville">
                    <?php
                           
                            foreach ($tabVilles as $ville) {
                                echo "<option value='".$ville."'>".$ville."</option>";

                            }
                       
                    ?>

                </select>
            </div>
            <br>
            <br>
            <br>
            <div class="form-group">
                <span class="glyphicon glyphicon-search"></span>
                &nbsp;
                <label for="Nom">Saisir tout ou partie du nom :</label>
                &nbsp;
                <input type="text" name="partieNom" class="form-control" required>
            </div>
                &nbsp;&nbsp;
            <div class="radio">
                <INPUT type= "radio" name="debutFin" value="debut" required> Début
                <INPUT type= "radio" name="debutFin" value="fin" required> Fin
                <INPUT type= "radio" name="debutFin" value="nimporte" required> Dans La Chaine
            </div>
                &nbsp;&nbsp;
            <button type="submit" class="btn btn-primary">Filtrer</button>
         </form> 
       
        
         <?php
         
         
            if((isset($_POST['debutFin']) && isset($_POST['ville']) && isset($_POST['partieNom'])) || isset($_POST['tri']))
            {
                
                if(!isset($_POST['tri']))
                {
                    $ovisiteurs = new Cvisiteurs();
                    $tabVisiteurs = $ovisiteurs->getTabVisiteursParNomEtVille($_POST['debutFin'],$_POST['partieNom'], $_POST['ville']);
                
                    $_SESSION["tabAtrie"] = $tabVisiteurs;
                    
                }
                 else {
                     $otri = new Ctri();
                     $tabVisiteurs = $otri->TriTableau($_SESSION["tabAtrie"]);
                     $_SESSION["tabAtrie"] = $tabVisiteurs;
                }
                //var_dump($tabVisiteurs);
                
                if($tabVisiteurs != null)
                {
                    echo "<br> <br>";
                    echo "<H1><span class='label label-warning'>Visiteurs médicaux filtrés par nom et ville :</span></H1>
                    <br> <br>
                    <form method='POST'>
                    <button type='submit' name='tri' class='btn btn-primary'>Tri Tableau</button> 
                    </form>
                    <br> <br>
                    <table class='table table-condensed'>
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>LOGIN</th>
                            <th>NOM</th>
                            <th>PRENOM</th>
                            <th>VILLE</th>
                          </tr>
                        </thead>
                        <tbody>";


                    foreach ($tabVisiteurs as $ovisiteur)
                    {

                            echo "<tr>";
                            echo "<td>".$ovisiteur->id."</td>"
                                 ."<td>".$ovisiteur->login."</td>"
                                 ."<td>".$ovisiteur->nom."</td>"
                                 ."<td>".$ovisiteur->prenom."</td>"
                                 ."<td>".$ovisiteur->ville."</td>"

                            ."</tr>";

                    }


                        echo "</tbody>";
                    echo "</table>";
                }
                if($tabVisiteurs == null)
                {
                    $errorMsg = "Il n'y a pas de visiteur répondant aux critères.";
                    
                    if(isset($errorMsg))
                    {
                         echo "<br><br><div class='alert alert-danger'>".$errorMsg."</div>";
                        
                    }
                    
                }
            }
            
        ?>
          
    </div>
    </body>
</html>
