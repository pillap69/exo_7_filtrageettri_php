<?php
require_once 'mesClasses/Cvisiteurs.php'; 

class Ctri {
    
  
    public function TriTableau($stabAtrier)
    {
          $ocollaccent = array (// on déclare le tableau ou on va stocker les valeurs ASCII ainsi que les e 
            201=>"e",
            232=>"e",
            233=>"e",
            235=>"e",
            ); 
        // on instancie la classe Cvisiteurs 
        $estTrie=false; // c'est la fonction Tri en C# début -----------------
        $longueur_tableau = count($stabAtrier); 
		
        
        
         for ($x = 0; $x < $longueur_tableau - 1 && !$estTrie; $x++)
        {
            $estTrie=true; 
            
            for ($i = 0; $i < $longueur_tableau - 1; $i++)
            {
                $x1 = substr($stabAtrier[$i]->nom,0,1);
                $x2 = substr($stabAtrier[$i+1]->nom, 0, 1);
                $chaine_minuscule = strtolower($x1);
                $chaine_minuscule2 = strtolower($x2);
                
               
                if (array_key_exists(ord($x1), $ocollaccent))
                    {
                        $chaine_minuscule = $ocollaccent[ord($x1)];
                    }
                if (array_key_exists(ord($x2), $ocollaccent))
                    {
                        $chaine_minuscule2 =$ocollaccent[ord($x2)]; 
                    }
                /*if (ord($chaine_minuscule) > ord($chaine_minuscule2) )
                    {
                        $this->permutation($stabAtrier, $i); 
                        
                        $estTrie=false; 
                    }*/
                //if (ord($chaine_minuscule) == ord($chaine_minuscule2))
                   // {
                        for ($z=0; $z< strlen($stabAtrier[$i]->nom); $z++)
                        {
                            $x=substr($stabAtrier[$i]->nom, $z, 1);
                            try
                            {
                                $y=substr($stabAtrier[$i+1]->nom, $z, 1);                   
                            } 
                            catch (Exception $ex) 
                            {
                                $y='!';
                            }
                            $seconde_chaine_minuscule = strtolower($x);
                            $seconde_chaine_minuscule2 = strtolower($y);
                            
                            
                            if (array_key_exists(ord($seconde_chaine_minuscule), $ocollaccent))
                            {
                                $seconde_chaine_minuscule  = $ocollaccent[ord($seconde_chaine_minuscule)]; 
                            }
                            if (array_key_exists(ord($seconde_chaine_minuscule2), $ocollaccent))
                                {
                                     $seconde_chaine_minuscule2  = $ocollaccent[ord($seconde_chaine_minuscule2)]; 
                                }
                            if (ord($seconde_chaine_minuscule) > ord($seconde_chaine_minuscule2))
                                {
                                    $this->permutation($stabAtrier,$i); 
                                    $estTrie=false; 
                                    break; 
                                }
                            else 
                                {
                                    if (ord($seconde_chaine_minuscule) < ord($seconde_chaine_minuscule2))
                                        {
                                            break ; 
                                        }
                                }
                        }
                   // } 
            }
        }
        return $stabAtrier;
    }
         
    private function permutation(&$stabAtrier, $si)
        {
            $temp = $stabAtrier[$si]; 
            $stabAtrier[$si]=$stabAtrier[$si+1]; 
            $stabAtrier[$si+1]= $temp; 
        }
}



