<html>
    <?php 
        require_once 'includes/head.php';        
        require_once './mesClasses/Cvisiteurs.php'; 
		require_once './mesClasses/Ctri.php';
        
        session_start();
    ?>
    <body>
        <?php
        $ovisiteurs = new Cvisiteurs();
        $tabVisiteurs = $ovisiteurs->getTabVisiteurs();
        $otri = new Ctri();
		$tabVisiteurTrie = $otri->TriTableau($tabVisiteurs);
        ?>
        
        <div class="container"> 
  <p>Tableau des visiteurs:</p>            
  <table class="table table-condensed">
    <thead>
      <tr>
        <th>ID</th>
        <th>LOGIN</th>
        <th>NOM</th>
        <th>PRENOM</th>
      </tr>
    </thead>
    <tbody>
        <?php
        
             foreach ($tabVisiteurTrie as $ovisiteur)
            {
                ?>        
                <tr>
                    <td><?=$ovisiteur->id ?></td>
                    <td> <?=$ovisiteur->login ?></td>
                    <td><?=$ovisiteur->nom ?></td>
                    <td><?=$ovisiteur->prenom?></td>
                 <?php
            
            ?>
                </tr>
             <?php
            }
            ?>      
    
    </tbody>
  </table>
</div>
    </body>
</html>
