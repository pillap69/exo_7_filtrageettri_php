<?php


/* ************ Classe métier Cvisiteur et Classe de contrôle Cvisiteurs **************** */
require_once 'mesClasses/Cdao.php';
class Cvisiteur
{

    public $id;
    public $login;
    public $nom;
    public $prenom;
    public $ville;
    public $mdp;
            
    function __construct($sid,$slogin,$snom,$sprenom,$mdp,$ville) //s pour send param envoyé
    {

        $this->id = $sid;
        $this->login = $slogin;
        $this->nom = $snom;
        $this->prenom = $sprenom;
        $this->mdp = $mdp;
        $this->ville = $ville;
    }
}


class Cvisiteurs 
{
 
    private $ocollVisiteurById;
    private $ocollVisiteurByLogin;
    private $tabVisiteur;
    private $tabVilleVisiteur;

    public function __construct()
    {
       
                  try {
                            $query = "SELECT * FROM visiteur";
                            $odao = new Cdao();
                            $lesVisiteurs = $odao->gettabDataFromSql($query);
                            
                            foreach ($lesVisiteurs as $unVisiteur) 
                                {
                                    $ovisiteur = new Cvisiteur($unVisiteur['id'],$unVisiteur['login'],$unVisiteur['nom'],$unVisiteur['prenom'],$unVisiteur['mdp'],$unVisiteur['ville']);
                                    $this->tabVisiteur[] = $ovisiteur;
				    $this->ocollVisiteurById[$unVisiteur['id']]  =  $ovisiteur;
				    $this->ocollVisiteurByLogin[$unVisiteur['login']] = $ovisiteur;
                                }
                                
                            $query = "SELECT distinct ville FROM visiteur";
                            
                            $lesVilles = $odao->gettabDataFromSql($query);
                            
                            foreach ($lesVilles as $uneVille)
                            {
                                $this->tabVilleVisiteur[] = $uneVille['ville'];
                                
                            }
                                
                      
                        }
                  catch(PDOException $e) {
                         $msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
                         die($msg);
                        }

    }



    function getVisiteurById($sid)
    {
        if(array_key_exists($sid, $this->ocollVisiteurById))
        {
            $ovisiteur = $this->ocollVisiteurById[$sid];
            return $ovisiteur;
        }
    }
    
    function getVilleVisiteur()
    {
        return $this->tabVilleVisiteur;
        
    }
    
    function getVisiteurByLogin($login)
    {
        if(array_key_exists($login, $this->ocollVisiteurByLogin))
        {
            $ovisiteur = $this->ocollVisiteurByLogin[$login];
            return $ovisiteur;
        }
    }
            
    function verifierInfosConnexion($username,$pwd)
    {
        foreach ($this->ocollVisiteurById as $ovisiteur)
        {
            if($ovisiteur->login == $username && $ovisiteur->mdp == $pwd)
            {
                return $ovisiteur;
            }                  
        }
        return null;
    }
    
    function getTabVisiteurs()
    {
        
        return $this->tabVisiteur;
    }
    
    function getTabVisiteursParNomEtVille($sdebutFin,$partieNom,$sville)
    {
        $tabVisiteursByVilleNom = null ;
        
        foreach ($this->tabVisiteur as $ovisiteur) {
            
            if(strtolower($ovisiteur->ville) == strtolower($sville))
            {
                if($sdebutFin == "debut")
                {
                    $nomExtrait = substr($ovisiteur->nom,0,strlen($partieNom));
                    
                    if(strtolower($nomExtrait) == strtolower($partieNom))
                    {
                        $tabVisiteursByVilleNom[] = $ovisiteur;
                    }                                      
                }
                if($sdebutFin == "fin")
                {
                    
                    $nomExtrait = substr($ovisiteur->nom,-strlen($partieNom),strlen($partieNom));
                    
                   if(strtolower($nomExtrait) == strtolower($partieNom))
                    {
                        $tabVisiteursByVilleNom[] = $ovisiteur;
                    }
                    
                } 
                
                if($sdebutFin == "nimporte")
                {
                    $i = 0;
                    $tab = str_split($ovisiteur->nom);
                    foreach ($tab as $caract) 
                    {
                        $nomExtrait = substr($ovisiteur->nom,$i,strlen($partieNom));

                        if(strtolower($nomExtrait) == strtolower($partieNom))
                        {
                            $tabVisiteursByVilleNom[] = $ovisiteur;
                            break;
                        } 

                        $i++;
                    }
               
                    
                }
            }
            
        }
        
       
        
        return $tabVisiteursByVilleNom;
    }
}


